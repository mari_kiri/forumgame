"""
Copyright (C) 2014, mari_kiri + AoS forum contributors

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

from constants import *
from roomclass import *

# This is the starting room.
# This syntax is nice for creating "trees" of rooms.
start_room = DarkRoom(NAME="Dark Room",
	EXITS={
		"north": Room(NAME="Light Room",
			DESC="It is light here. You are not likely to be eaten by a grue.",
			EXITS={}),
	})

# A side room.
# This is one way to create a cycle.
# Note, when you add anything to the EXITS dict *in the constructor*, it'll automatically link back.
# However if you do add_exit, it won't link back.
side_room = DarkRoom(NAME="Weird Room",
	DESC="It is dark here. And non-euclidean. You are likely to be eaten by Euclid's grue.",
	EXITS={
		"east": start_room,
		"west": start_room.EXITS["north"],
	})

