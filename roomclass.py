"""
Copyright (C) 2014, mari_kiri + AoS forum contributors

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

from constants import *

# The current room.
ROOM = None

# If you need to move to another room, use this.
# Don't roll your own.
def roomchange(new_room):
	global ROOM

	if ROOM != None:
		ROOM.on_exit()

	ROOM = new_room
	ROOM.on_entry()


# Anything you add here will be true for all rooms unless other types of rooms override your things.
class Room:
	def __init__(self, NAME=None, DESC=None, EXITS={}):
		if NAME != None:
			self.NAME = NAME
		if DESC != None:
			self.DESC = DESC

		self.EXITS = EXITS
		self.COMMANDS = {}
		for (k, v) in tuple(EXITS.iteritems()):
			v.add_exit(INVEXIT[k], self)
		self.all_commands()

	# Want to add new commands? Override this. PROPERLY.
	# See DarkRoom for how this is done. (Even though it doesn't use it yet.)
	def all_commands(self):
		self.add_command(self.go)
		self.add_command(self.look)
		self.add_command(self.sleep)

	# Here are some COMMANDS.
	# They all have the (self, args=[], largs=[]) pattern to them.
	def go(self, args=[], largs=[]):
		if len(args) < 1:
			print "Where do you want to go?"
			return

		direction = args[0].lower()
		if direction in ALTDIRS:
			direction = ALTDIRS[direction]

		if direction in self.EXITS:
			print "You head %s." % direction
			roomchange(self.EXITS[direction])
		else:
			print "You can't go that way."

	def look(self, args=[], largs=[]):
		# Show a description.
		print self.DESC

		# Show the exits.
		elist = list(self.EXITS)
		if len(elist) == 0:
			pass # There are no exits.
		elif len(elist) == 1:
			print "Exits are " + elist[0] + "."
		else:
			print "Exits are " + ", ".join(elist[:-1]) + ", and " + elist[-1] + "."

	def sleep(self, args=[], largs=[]):
		print "Well, that was a waste of time."
		print
		self.look()

	# Override this PROPERLY if you want to.
	def on_entry(self):
		print "-"*30
		print self.NAME
		self.look()

	# Override this PROPERLY if you want to.
	def on_exit(self):
		pass

	# Call this whenever you need to add an exit in a direction.
	def add_exit(self, direction, room):
		self.EXITS[direction] = room

	# Call this whenever you need to add a command for this room.
	def add_command(self, cmd, name=None):
		if self.COMMANDS == None:
			self.COMMANDS = {}
		if name == None:
			name = cmd.__name__
		self.COMMANDS[name] = cmd

	# The main loop will call this.
	def do_command(self, s):
		s = s.strip()
		command, _, s = s.partition(" ")

		# Check if we have a command.
		command = command.lower()
		if command == "":
			return
		if command not in self.COMMANDS:
			print "I don't understand that."
			print
			return

		# args contains single space-separated "words".
		# largs contains the remainder of the string from a point.
		args = []
		largs = []
		s = s.strip()
		if s != "":
			while " " in s:
				largs.append(s)
				arg, _, s = s.partition(" ")
				s = s.strip()
				args.append(arg)

			# Let's not forget the last argument.
			largs.append(s)
			args.append(s)
		
		# We now call this command.
		self.COMMANDS[command](args, largs)
		print

# Here's an example of a dark room.
class DarkRoom(Room):
	NAME = "Dark Room"
	DESC = "It is dark here. You are likely to be eaten by a grue."

	def all_commands(self):
		# You can add extra commands here.
		return Room.all_commands(self)

	def sleep(self, args=[], largs=[]):
		print "You have been eaten by a grue."
		raise PlayerDead()

